package com.springboot.crud.exception;

@SuppressWarnings("serial")
public class InvalidAccountException extends Exception {
	public InvalidAccountException(String message) {
		super(message);
	}
}
