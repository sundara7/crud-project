package com.springboot.crud.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springboot.crud.entity.Account;
import com.springboot.crud.exception.AccountNotFoundException;
import com.springboot.crud.exception.InvalidAccountException;
import com.springboot.crud.repository.AccountRepository;
import com.springboot.crud.service.AccountService;

@Service
public class AccountServiceImpl implements AccountService {

	@Autowired
	private AccountRepository repository;

	@Override
	public List<Account> fetchAll() {
		return (List<Account>) repository.findAll();
	}

	@Override
	public Account create(Account account) {
		return repository.save(account);
	}

	@Override
	public Account update(Account updated) throws AccountNotFoundException, InvalidAccountException {
		Optional<Account> account = getAccountById(updated.getId());
		if (account.isPresent()) {
			Account existing = getUpdatedAccount(updated, account);
			return repository.save(existing);
		} else {
			throw new AccountNotFoundException("Account not found with id: "
					+ updated.getId());
		}
	}

	@Override
	public void delete(Long accountId) throws AccountNotFoundException, InvalidAccountException {
		Optional<Account> account = getAccountById(accountId);
		if (account.isPresent()) {
			repository.delete(account.get());
		} else {
			throw new AccountNotFoundException("Account not found with id: "
					+ accountId);
		}
	}

	@Override
	public Account fetchById(Long id) throws AccountNotFoundException, InvalidAccountException {
		Optional<Account> account = getAccountById(id);
		if (account.isPresent()) {
			return account.get();
		} else {
			throw new AccountNotFoundException("Account not found with id: "
					+ id);
		}
	}

	private Optional<Account> getAccountById(Long accountId) throws InvalidAccountException {
		if (accountId == null) {
			throw new InvalidAccountException("Account Id cannot be null");
		}
		return repository.findById(accountId);
	}

	private Account getUpdatedAccount(Account updated,
			Optional<Account> account) {
		Account existing = account.get();
		if (updated.getName() != null) {
			existing.setName(updated.getName());
		}
		if (updated.getBranch() != null) {
			existing.setBranch(updated.getBranch());
		}
		if (updated.getBalance() != null) {
			existing.setBalance(updated.getBalance());
		}
		return existing;
	}

}
