package com.springboot.crud.service;

import java.util.List;

import com.springboot.crud.entity.Account;
import com.springboot.crud.exception.AccountNotFoundException;
import com.springboot.crud.exception.InvalidAccountException;

public interface AccountService {
	List<Account> fetchAll();
	
	Account fetchById(Long id) throws AccountNotFoundException, InvalidAccountException;

	Account create(Account account);

	Account update(Account account) throws AccountNotFoundException, InvalidAccountException;

	void delete(Long accountId) throws AccountNotFoundException, InvalidAccountException;
}
