package com.springboot.crud.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.crud.entity.Account;
import com.springboot.crud.exception.AccountNotFoundException;
import com.springboot.crud.exception.InvalidAccountException;
import com.springboot.crud.service.AccountService;

@RestController
@RequestMapping(value = "/api/account")
public class AccountController {

	@Autowired
	private AccountService service;

	@GetMapping("/fetchAll")
	public List<Account> fetchAllAccounts() {
		return service.fetchAll();
	}

	@GetMapping("/fetchById/{id}")
	public Account fetchAccountById(@PathVariable("id") Long id)
			throws AccountNotFoundException, InvalidAccountException {
		return service.fetchById(id);
	}

	@PostMapping("/create")
	public Account createAccount(@RequestBody Account account) {
		return service.create(account);
	}

	@PutMapping("/update")
	public Account updateAccount(@RequestBody Account account)
			throws AccountNotFoundException, InvalidAccountException {
		return service.update(account);
	}

	@DeleteMapping("/delete/{id}")
	public String deleteAccount(@PathVariable("id") Long id)
			throws AccountNotFoundException, InvalidAccountException {
		service.delete(id);
		return "\"" + "Account with id: " + id
				+ " has been deleted successfully" + "\"";
	}
}
