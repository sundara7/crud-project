package com.springboot.crud.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.springboot.crud.entity.Account;

@Repository
public interface AccountRepository extends CrudRepository<Account, Long> {

}
