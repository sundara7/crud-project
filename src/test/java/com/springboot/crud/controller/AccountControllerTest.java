package com.springboot.crud.controller;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.springboot.crud.entity.Account;
import com.springboot.crud.exception.AccountNotFoundException;
import com.springboot.crud.exception.InvalidAccountException;
import com.springboot.crud.service.impl.AccountServiceImpl;

@RunWith(SpringJUnit4ClassRunner.class)
@WebMvcTest(value = AccountController.class)
public class AccountControllerTest {

	@Autowired
	private MockMvc mvc;

	@MockBean
	private AccountServiceImpl service;

	@InjectMocks
	private AccountController controller;

	@Autowired
	private WebApplicationContext context;

	@Before
	public void setup() {
		mvc = MockMvcBuilders.webAppContextSetup(context).build();
	}

	@Test
	public void returnAllAccountsWhenFetchingAllAccounts() throws Exception {
		Mockito.when(service.fetchAll()).thenReturn(getAccounts());

		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
				"/api/account/fetchAll").accept(MediaType.APPLICATION_JSON);

		MvcResult result = mvc.perform(requestBuilder).andReturn();

		assertEquals(200, result.getResponse().getStatus());

		String expected = "[{\"id\":1,\"name\":\"Sundar\",\"branch\":\"Chennai\",\"balance\":10000.0},{\"id\":2,\"name\":\"Rajan\",\"branch\":\"Chennai\",\"balance\":100000.0},{\"id\":3,\"name\":\"Basha\",\"branch\":\"Chennai\",\"balance\":5000.0}]";

		JSONAssert.assertEquals(expected, result.getResponse()
				.getContentAsString(), false);
	}

	@Test
	public void returnAccountWhenFetchingAccountByGivenId() throws Exception {
		Mockito.when(service.fetchById(1L)).thenReturn(
				getAccount(1L, "Sundar", 1000.00));

		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
				"/api/account/fetchById/1").accept(MediaType.APPLICATION_JSON);

		MvcResult result = mvc.perform(requestBuilder).andReturn();

		assertEquals(200, result.getResponse().getStatus());

		String expected = "{\"id\":1,\"name\":\"Sundar\",\"branch\":\"Chennai\",\"balance\":1000.0}";

		JSONAssert.assertEquals(expected, result.getResponse()
				.getContentAsString(), false);
	}

	@Test
	public void returnSuccessResponseWhenCreatingNewAccount() throws Exception {
		String accountJson = "{\"name\":\"Sundar\",\"branch\":\"Chennai\",\"balance\":1000.0}";
		RequestBuilder requestBuilder = MockMvcRequestBuilders
				.post("/api/account/create").accept(MediaType.APPLICATION_JSON)
				.content(accountJson).contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mvc.perform(requestBuilder).andReturn();

		assertEquals(200, result.getResponse().getStatus());
	}

	@Test
	public void returnSuccessResponseWhenUpdatingExistingAccount()
			throws Exception {
		String accountJson = "{\"id\":\"1\",\"name\":\"Updated\",\"branch\":\"Updated\",\"balance\":100.0}";
		RequestBuilder requestBuilder = MockMvcRequestBuilders
				.put("/api/account/update").accept(MediaType.APPLICATION_JSON)
				.content(accountJson).contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mvc.perform(requestBuilder).andReturn();

		assertEquals(200, result.getResponse().getStatus());
	}

	@Test
	public void returnDeletedResponseWhenDeletingGivenAccountId()
			throws Exception {
		Mockito.when(service.fetchById(1L)).thenReturn(
				getAccount(1L, "Sundar", 1000.00));

		RequestBuilder requestBuilder = MockMvcRequestBuilders.delete(
				"/api/account/delete/1").accept(MediaType.APPLICATION_JSON);

		MvcResult result = mvc.perform(requestBuilder).andReturn();

		assertEquals(200, result.getResponse().getStatus());

		String expected = "\""
				+ "Account with id: 1 has been deleted successfully" + "\"";
		JSONAssert.assertEquals(expected, result.getResponse()
				.getContentAsString(), false);
	}

	@Test
	public void returnNotFoundResponseWhenGivenAccountIdNotFound()
			throws Exception {
		Mockito.when(service.fetchById(2L)).thenThrow(
				AccountNotFoundException.class);
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
				"/api/account/fetchById/2").accept(MediaType.APPLICATION_JSON);

		MvcResult result = mvc.perform(requestBuilder).andReturn();

		assertEquals(404, result.getResponse().getStatus());
	}

	@Test
	public void returnBadRequestResponseWhenGivenAccountIdIsNull()
			throws Exception {
		Mockito.when(service.fetchById(null)).thenThrow(
				InvalidAccountException.class);
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
				"/api/account/fetchById/null").accept(
				MediaType.APPLICATION_JSON);

		MvcResult result = mvc.perform(requestBuilder).andReturn();

		assertEquals(400, result.getResponse().getStatus());
	}

	private List<Account> getAccounts() {
		List<Account> accounts = new ArrayList<Account>();
		Account account = getAccount(1L, "Sundar", 10000.00);
		Account account1 = getAccount(2L, "Rajan", 100000.00);
		Account account2 = getAccount(3L, "Basha", 5000.00);
		accounts.add(account);
		accounts.add(account1);
		accounts.add(account2);
		return accounts;
	}

	private Account getAccount(Long id, String name, Double balance) {
		Account account = new Account();
		account.setId(id);
		account.setName(name);
		account.setBranch("Chennai");
		account.setBalance(balance);
		return account;
	}
}
