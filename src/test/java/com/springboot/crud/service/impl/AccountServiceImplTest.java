package com.springboot.crud.service.impl;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.springboot.crud.entity.Account;
import com.springboot.crud.exception.AccountNotFoundException;
import com.springboot.crud.exception.InvalidAccountException;
import com.springboot.crud.repository.AccountRepository;

@RunWith(SpringJUnit4ClassRunner.class)
public class AccountServiceImplTest {
	@Mock
	private AccountRepository repository;

	@InjectMocks
	private AccountServiceImpl service;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void returnAllAccountsWhenFetchAll() throws Exception {
		List<Account> accounts = getAccounts();
		Mockito.when(repository.findAll()).thenReturn(accounts);
		List<Account> actual = service.fetchAll();

		assertEquals(3, actual.size());
	}

	@Test
	public void returnCreatedAccountWhenCreate() throws Exception {
		Account account = getAccount(1L, "Sundar", 10000.00);
		Mockito.when(repository.save(account)).thenReturn(account);

		Account actual = service.create(account);
		Mockito.verify(repository, Mockito.atLeastOnce()).save(Mockito.any());

		assertEquals("Sundar", actual.getName());
		assertEquals(Double.valueOf(10000.00), actual.getBalance());
	}

	@Test
	public void returnUpdatedAccountWhenUpdatingAccountNameAndBalance() throws Exception {
		Account account = getAccount(1L, "Sundar", 10000.00);
		Optional<Account> optional = Optional.of(account);
		Mockito.when(repository.findById(1L)).thenReturn(optional);

		Account updated = getAccount(1L, "Updated Name", 2000.00);
		updated.setBranch(null);
		Mockito.when(repository.save(account)).thenReturn(updated);
		Account actual = service.update(updated);

		Mockito.verify(repository, Mockito.atLeastOnce()).save(Mockito.any());
		assertEquals("Updated Name", actual.getName());
		assertEquals(Double.valueOf(2000.00), actual.getBalance());
	}

	@Test
	public void returnUpdatedAccountWhenUpdatingBranch() throws Exception {
		Account account = getAccount(1L, "Sundar", 10000.00);
		Optional<Account> optional = Optional.of(account);
		Mockito.when(repository.findById(1L)).thenReturn(optional);

		Account updated = getAccount(1L, null, null);
		updated.setBranch("Updated Branch");
		Mockito.when(repository.save(account)).thenReturn(updated);
		Account actual = service.update(updated);

		Mockito.verify(repository, Mockito.atLeastOnce()).save(Mockito.any());
		assertEquals("Updated Branch", actual.getBranch());
	}
	
	@Test
	public void verifyAccountDeletionGivenAccountIdWhenDelete() throws Exception {
		Account account = getAccount(1L, "Sundar", 10000.00);
		Optional<Account> optional = Optional.of(account);
		Mockito.when(repository.findById(1L)).thenReturn(optional);
		
		service.delete(1L);
		
		Mockito.verify(repository, Mockito.atLeastOnce()).delete(account);
	}
	
	@Test(expected = AccountNotFoundException.class)
	public void throwsAccountNotFoundExceptionWhenGivenAccountIdIsNotFoundForDelete()
			throws Exception {
		service.delete(1L);
	}

	@Test(expected = InvalidAccountException.class)
	public void throwsInvalidAccountExceptionWhenGivenAccountIdIsNullForDelete()
			throws Exception {
		service.delete(null);
	}

	@Test(expected = AccountNotFoundException.class)
	public void throwsAccountNotFoundExceptionWhenGivenAccountIdIsNotFoundForUpdate()
			throws Exception {
		service.update(getAccount(1L, "Test", 100.00));
	}

	@Test(expected = InvalidAccountException.class)
	public void throwsInvalidAccountExceptionWhenGivenAccountIdIsNullForUpdate()
			throws Exception {
		service.update(getAccount(null, "Test", 100.00));
	}

	@Test
	public void returnAccountForGivenAccountId() throws Exception {
		Optional<Account> optional = Optional.of(getAccount(1L, "Sundar",
				10000.00));
		Mockito.when(repository.findById(1L)).thenReturn(optional);
		Account actual = service.fetchById(1L);

		assertEquals(Long.valueOf(1), actual.getId());
		assertEquals("Sundar", actual.getName());
		assertEquals(Double.valueOf(10000.00), actual.getBalance());
	}

	@Test(expected = AccountNotFoundException.class)
	public void throwsAccountNotFoundExceptionWhenThereIsNoAccountIdFound()
			throws Exception {
		Optional<Account> optional = Optional.of(getAccount(1L, "Sundar",
				10000.00));
		Mockito.when(repository.findById(1L)).thenReturn(optional);
		service.fetchById(2L);
	}

	@Test(expected = InvalidAccountException.class)
	public void throwsInvalidAccountExceptionWhenGivenAccountIdIsNull()
			throws Exception {
		service.fetchById(null);
	}

	private List<Account> getAccounts() {
		List<Account> accounts = new ArrayList<Account>();
		Account account = getAccount(1L, "Sundar", 10000.00);
		Account account1 = getAccount(2L, "Rajan", 100000.00);
		Account account2 = getAccount(3L, "Basha", 5000.00);
		accounts.add(account);
		accounts.add(account1);
		accounts.add(account2);
		return accounts;
	}

	private Account getAccount(Long id, String name, Double balance) {
		Account account = new Account();
		account.setId(id);
		account.setName(name);
		account.setBranch("Chennai");
		account.setBalance(balance);
		return account;
	}
}
